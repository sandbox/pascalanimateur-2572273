<?php
/**
 * @file
 * Internationalization (i18n) hooks
 */

/**
 * Implements hook_i18n_object_info().
 */
function multilingual_taxonomy_i18n_object_info() {
  $info['taxonomy_vocabulary'] = array(
    'title' => t('Vocabulary'),
    'entity' => 'taxonomy_vocabulary',
    'key' => 'vid',
    'placeholders' => array(
      '%taxonomy_vocabulary_machine_name' => 'machine_name',
    ),
    // Auto generate edit path
    'edit path' => 'admin/structure/taxonomy/%taxonomy_vocabulary_machine_name/edit',
    // Auto-generate translate tab
    'translate tab' => 'admin/structure/taxonomy/%taxonomy_vocabulary_machine_name/translate',
    // We can easily list all these objects
    'list callback' => 'taxonomy_get_vocabularies',
    // Metadata for string translation
    'string translation' => array(
      'textgroup' => 'taxonomy',
      'type' => 'vocabulary',
      'properties' => array(
        'name' => t('Name'),
        'description' => t('Description'),
      ),
    ),
  );
  return $info;
}

/**
 * Implements hook_i18n_string_info()
 */
function multilingual_taxonomy_i18n_string_info() {
  $groups['taxonomy'] = array(
    'title' => t('Taxonomy'),
    'description' => t('Vocabulary titles and term names for localizable vocabularies.'),
    'format' => FALSE, // This group doesn't have strings with format
    'list' => FALSE, // This group cannot list all strings
    'refresh callback' => 'multilingual_taxonomy_i18n_string_refresh',
  );
  return $groups;
}
